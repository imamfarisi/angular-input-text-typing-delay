import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  data : string = ""
  dataFilter : string = ""
  filterTimeout : any

  dataChange(data : any) : void {
    this.dataFilter = 'typing .....'
    
    if(this.filterTimeout) {
        clearTimeout(this.filterTimeout)
    }
    
    this.filterTimeout = setTimeout(() => {
        this.dataFilter = data
    }, 300)
  }
}
